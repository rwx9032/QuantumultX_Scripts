// script-response-body

[rewrite_local]
(^https:\/\/pingtas\.qq\.com\/webview\/pingd\?dm=c\.pc\.qq\.com&pvi=\d+&si=s\d+&url=\/ios\.html\?url%3d)(http.*)(%26level.*%26level.*) url script-response-body https://gitlab.com/rwx9032/QuantumultX_Scripts/-/raw/main/script/QQ_redirect.js
(^https:\/\/c\.pc\.qq\.com\/ios\.html\?url=)(http.*)(&level=.*) url script-response-body https://gitlab.com/rwx9032/QuantumultX_Scripts/-/raw/main/script/QQ_redirect.js
(^https:\/\/c\.pc\.qq\.com\/index\.html\?pfurl=)(http.*)(&pfuin=.*) url script-response-body https://gitlab.com/rwx9032/QuantumultX_Scripts/-/raw/main/script/QQ_redirect.js
(^https:\/\/cgi\.connect\.qq\.com\/qqconnectopen\/get_urlinfoForQQV2\?url=)(http.*) url script-response-body https://gitlab.com/rwx9032/QuantumultX_Scripts/-/raw/main/script/QQ_redirect.js
(^https:\/\/pingtas\.qq\.com\/webview\/pingd\?dm=c\.pc\.qq\.com&pvi=\d+&si=s\d+&url=\/middlem\.html\?pfurl%3d)(http.*)(%26pfuin%3d.*%26pfuin%3d.*) url script-response-body https://gitlab.com/rwx9032/QuantumultX_Scripts/-/raw/main/script/QQ_redirect.js
(^https:\/\/c\.pc\.qq\.com\/middlem\.html\?pfurl=)(http.*)(&pfuin=.*) url script-response-body https://gitlab.com/rwx9032/QuantumultX_Scripts/-/raw/main/script/QQ_redirect.js

[mitm]
hostname = pingtas.qq.com, c.pc.qq.com, cgi.connect.qq.com

// 获取响应的 url 编码
var url = $response.url;
// 判断是否有 url 编码
if (url) {
  // 使用正则表达式匹配并提取指定部分
  var matchResult = url.match(/^https:\/\/pingtas\.qq\.com\/webview\/pingd\?dm=c\.pc\.qq\.com&pvi=\d+&si=s\d+&url=\/ios\.html\?url%3d(http.*)(%26level.*%26level.*)|^https:\/\/c\.pc\.qq\.com\/ios\.html\?url=(http.*)(&level=.*)|^https:\/\/c\.pc\.qq\.com\/index\.html\?pfurl=(http.*)(&pfuin=.*)|^https:\/\/cgi\.connect\.qq\.com\/qqconnectopen\/get_urlinfoForQQV2\?url=(http.*)|^https:\/\/pingtas\.qq\.com\/webview\/pingd\?dm=c\.pc\.qq\.com&pvi=\d+&si=s\d+&url=\/middlem\.html\?pfurl%3d(http.*)(%26pfuin%3d.*%26pfuin%3d.*)|^https:\/\/c\.pc\.qq\.com\/middlem\.html\?pfurl=(http.*)(&pfuin=.*)/);
  // 提取匹配到的部分
  if (matchResult) {
    url = matchResult[1] || matchResult[3] || matchResult[5] || matchResult[7] || matchResult[9] || matchResult[11];
  }
  // 使用 decodeURIComponent 函数解码
  var decoded_url = decodeURIComponent(url);
  // 输出日志
  console.log("解码前的 url: " + url);
  console.log("解码后的 url: " + decoded_url);
  // 返回 302 状态码和解码后的 url
  $done(JSON.stringify({status: 302, url: decoded_url}));
} else {
  // 如果没有 url 编码，直接结束
  $done();
}
